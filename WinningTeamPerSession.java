package io.mountblue;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class WinningTeamPerSession {

    public static void calculate(BufferedReader matchReader) throws IOException {
        String matchInfo = matchReader.readLine();
        matchInfo = matchReader.readLine();
        ArrayList<Integer> years = new ArrayList<Integer>();
        ArrayList<String> winners = new ArrayList<String>();
        while (matchInfo != null) {
            String[] matchInfoArray = matchInfo.split(",");
            int year = Integer.parseInt(matchInfoArray[1]);
            String winner = matchInfoArray[10];
            years.add(year);
            winners.add(winner);
            matchInfo = matchReader.readLine();
        }
        years.get(0);
        int winnerFirstIndex = 0;
        int firstYear = years.get(0);
        int yearLastIndex = years.lastIndexOf(firstYear);
        while (yearLastIndex < years.size()) {

            HashMap<String, Integer> winCounter = new HashMap<String, Integer>();
            for (int index = winnerFirstIndex; index <= yearLastIndex; index++) {
                String winner = winners.get(index);
                winCounter.put(winner, winCounter.containsKey(winner) ? (winCounter.get(winner) + 1) : 1);
            }
            for (Map.Entry<String, Integer> entry : winCounter.entrySet())
                System.out.println(firstYear + " : " + entry.getKey() + " : " + entry.getValue());
            yearLastIndex++;
            if (yearLastIndex < years.size()) {
                firstYear = years.get(yearLastIndex);
                winnerFirstIndex = yearLastIndex;
                yearLastIndex = years.lastIndexOf(firstYear);
            }
        }
    }

}
