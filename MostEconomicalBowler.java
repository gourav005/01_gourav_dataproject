package io.mountblue;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class MostEconomicalBowler { public static void calculate(BufferedReader matchReader, BufferedReader deliveryReader) throws IOException
{
    String matchInfo =matchReader.readLine();
    matchInfo=matchReader.readLine();
    HashMap<String,Integer> bowlerCounter=new HashMap<String,Integer>();
    TreeSet<Integer> teamIdies=new TreeSet<Integer>();
    while(matchInfo!=null)
    {
        String[] matchInfoArray=matchInfo.split(",");
        int year=Integer.parseInt(matchInfoArray[1]);
        if(year==2015)
        {
            teamIdies.add(Integer.parseInt(matchInfoArray[0]));
        }
        matchInfo=matchReader.readLine();
        matchInfo=matchReader.readLine();
    }
    matchInfo=deliveryReader.readLine();
    matchInfo=deliveryReader.readLine();
    while(matchInfo!=null)
    {
        String[] matchInfoArray=matchInfo.split(",");
        int matchId=Integer.parseInt(matchInfoArray[0]);
        int totalRuns=Integer.parseInt(matchInfoArray[9])+Integer.parseInt(matchInfoArray[10])+Integer.parseInt(matchInfoArray[12]);
        totalRuns+=Integer.parseInt(matchInfoArray[13])+Integer.parseInt(matchInfoArray[15])+Integer.parseInt(matchInfoArray[16]);
        if (teamIdies.contains(matchId))
        {
            String player=matchInfoArray[8];
            if(bowlerCounter.containsKey((player)))
                bowlerCounter.put(player,bowlerCounter.get(player)+totalRuns);
            else
                bowlerCounter.put(player,totalRuns);
        }
        matchInfo=deliveryReader.readLine();
    }
    int minimumrun=Integer.MAX_VALUE;
    String mostEconomicalBowler="";
    for(Map.Entry<String,Integer> bowlerIterator : bowlerCounter.entrySet())
        if(minimumrun>bowlerIterator.getValue())
        {
            minimumrun=(Integer)bowlerIterator.getValue();
            mostEconomicalBowler=(String)bowlerIterator.getKey();
        }
    System.out.println("The most Economic bowler in 2015  is ; "+mostEconomicalBowler+" AND "+"Who had given :"+minimumrun+" RUNS");
}
}
