package io.mountblue;

import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;

public class YearAndWins {

    public static void calculate(BufferedReader matchReader) throws IOException {
        HashMap<Integer,Integer> matchCount=new HashMap<Integer, Integer>();
        String row=matchReader.readLine();
            row=matchReader.readLine();
        while(row!=null)
               {
                        String[] matchInfo=row.split(",");
                        int  year=Integer.parseInt(matchInfo[1]);
                        if(matchCount.containsKey(year))
                            matchCount.put(year,matchCount.get(year)+1);
                        else
                            matchCount.put(year,1);
                        row=matchReader.readLine();
                }
        for(Map.Entry<Integer,Integer> entry:matchCount.entrySet())
        {
            System.out.println(entry.getKey()+": Total No Of Matches : "+entry.getValue());
        }
    }
}
