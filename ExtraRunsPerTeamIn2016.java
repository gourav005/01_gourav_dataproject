package io.mountblue;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class ExtraRunsPerTeamIn2016 {
    public static void calculate(BufferedReader matchReader, BufferedReader deliveryReader) throws IOException
    {
        String matchInfo =matchReader.readLine();
        matchInfo=matchReader.readLine();
        HashMap<String,Integer> runCounter=new HashMap<String,Integer>();
        TreeSet<Integer> teamIdies=new TreeSet<Integer>();
        while(matchInfo!=null)
        {
            String[] matchInfoArray=matchInfo.split(",");
            int year=Integer.parseInt(matchInfoArray[1]);
            if(year==2016)
            {
                teamIdies.add(Integer.parseInt(matchInfoArray[0]));
            }
            matchInfo=matchReader.readLine();
            matchInfo=matchReader.readLine();
        }
        //System.out.println(teamIdies);
        matchInfo=deliveryReader.readLine();
        matchInfo=deliveryReader.readLine();
        while(matchInfo!=null)
        {
            String[] matchInfoArray=matchInfo.split(",");
            int matchId=Integer.parseInt(matchInfoArray[0]);
            if(teamIdies.contains(matchId))
            {
                int extaRuns=Integer.parseInt(matchInfoArray[17]);
                String team=matchInfoArray[3];
                runCounter.put(team,runCounter.containsKey(team)?runCounter.get(team)+extaRuns:extaRuns);
            }
            matchInfo=deliveryReader.readLine();
        }
        for(Map.Entry<String,Integer> mapIterator:runCounter.entrySet())
            System.out.println(mapIterator.getKey()+" : "+mapIterator.getValue());
    }

}
